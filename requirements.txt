Django==3.2.5
django-bootstrap4==3.0.1
django-crispy-forms==1.12.0
django-widget-tweaks==1.4.8
mysqlclient==2.0.3
mysql-connector-python==8.0.25
django-simple-history==3.0.0
xlwt==1.3.0