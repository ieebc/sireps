import xlwt
from django.shortcuts import get_object_or_404, render, redirect
from .models import *
from .forms import RegistroForm
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, Http404
from django.core import serializers

# Create your views here.
def api_registros(request):
    registros = Registro.objects.all()
    lista_registros = []

    for registro in registros:
        registro_formato = {}        
        registro_formato['tipo'] = registro.get_tipo_display()
        registro_formato['nombre'] = registro.nombre
        registro_formato['clave'] = registro.clave
        registro_formato['sexo'] = registro.get_sexo_display()
        registro_formato['cargo'] = registro.get_cargo_display()
        registro_formato['relacion'] = registro.get_relacion_display()
        registro_formato['partido'] = registro.partido        
        registro_formato['ambito'] = registro.get_ambito_display()        
        registro_formato['incidencia'] = registro.incidencia
        registro_formato['expediente'] = registro.expediente
        registro_formato['organo'] = registro.organo
        registro_formato['fecha_resolucion'] = registro.fecha_resolucion
        registro_formato['conducta'] = registro.conducta
        registro_formato['sancion'] = registro.get_sancion_display()
        registro_formato['calificacion'] = registro.get_calificacion_display()
        registro_formato['enlace'] = registro.enlace
        registro_formato['permanencia'] = registro.permanencia
        registro_formato['reincidencia'] = registro.get_reincidencia_display()
        registro_formato['resolucion'] = registro.get_resolucion_display()
        lista_registros.append(registro_formato)

    return JsonResponse(lista_registros, safe=False)

def registros_publicos(request):    
    registros = Registro.objects.all()
    context = { 'registros': registros }
    return render(request, 'sireps/registros_publicos.html', context)


def registros(request):    
    if request.user.is_authenticated:
        registros = Registro.objects.all()
        context = { 'registros': registros }
        return render(request, 'sireps/registros.html', context)
    else:
        return redirect('login')

@login_required
def registro_modal(request, pk):
    if request.is_ajax():
        registro = get_object_or_404(Registro, pk = pk)
        context = { 'registro': registro }
        response = serializers.serialize("json", [registro])
        return render(request, 'sireps/modal-confirmacion.html', context)
    raise Http404('Page not found')
   

@login_required
def registro_nuevo(request):
    current_user = get_object_or_404(User, pk=request.user.pk)
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            registro = form.save(commit=False)
            registro.user = current_user
            registro.save()
            messages.success(request, 'Registro exitoso!')
            return redirect('registros')
    else:
        form = RegistroForm()
    return render(request, 'sireps/registro-nuevo.html', {'form': form})

@login_required
def registro_editar(request, pk):
    registro = get_object_or_404(Registro, pk = pk)
    if request.method == 'POST':
        form = RegistroForm(request.POST, instance=registro)
        if form.is_valid():
            form.save()
            messages.success(request, 'Edición de registro exitosa!')
            return redirect('registros')
    else:
        form = RegistroForm(instance=registro)
    return render(request, 'sireps/registro-editar.html', {'form': form})

@login_required
def registro_eliminar(request, pk):
    registro = get_object_or_404(Registro, pk = pk)
    registro.delete()
    messages.success(request, 'Eliminación de registro exitosa!')
    return redirect('registros')

def exportar_registros_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="REPS_DatosPúblicos.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Registros')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Nombre', 'Cargo', 'Sexo', 'Ámbito Territorial', 'Número de expediente', 'Relación con la víctima',  'Incidencia en el proceso electoral', 'Órgano resolutor', 
    'Fecha de la resolución', 'Conducta', 'Sanción', 'Permanencia', 'Reincidencia de la conducta', 'Resolución penal', 'Documento enlace',]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    registros = Registro.objects.all()
    for row in registros:
        row_num += 1
        row = [row.nombre, row.get_cargo_display(), row.get_sexo_display(), row.get_ambito_display(), row.expediente, row.get_relacion_display(), row.incidencia, row.organo, row.fecha_resolucion.strftime('%d/%m/%Y'), 
        row.conducta, row.get_sancion_display(), row.permanencia.strftime('%d/%m/%Y'), row.get_reincidencia_display(), row.get_resolucion_display(), row.enlace]
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response