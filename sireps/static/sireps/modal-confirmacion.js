$(document).ready(function(){
  $( ".delete-modal" ).click(function() {
    var endpoint = $(this).attr("data-url");
    $.ajax({
      url: endpoint,
      type: "GET",
      success(response) {
        console.log(response)
        $("#deleteModalDiv").html(response);
        $('#deleteModal').modal('show')
      },
    });   
  });
});