from django.db import models
from simple_history.models import HistoricalRecords
from django.core.validators import MinLengthValidator

# Create your models here.
class Registro(models.Model):

    # TIPO DE REGISTRO
    INMEDIATO = 'IN'
    REINCIDENTE = 'RE'
    TIPOS = [
        (INMEDIATO, 'Inmediato'),
        (REINCIDENTE, 'Reincidente'),
    ]

    tipo = models.CharField(
        max_length=2,
        choices=TIPOS,
        default=INMEDIATO,
    )

    # PERSONA INFRACTORA
    nombre = models.CharField(max_length=255)
    clave = models.CharField(max_length=18, validators=[MinLengthValidator(18)], null=True)

    HOMBRE = 'H'
    MUJER = 'M'
    SEXOS = [
        (HOMBRE, 'Hombre'),
        (MUJER, 'Mujer'),
    ]

    sexo = models.CharField(
        max_length=1,
        choices=SEXOS,
        default=HOMBRE,
    )

    FUNCIONARIADO = 'FP'
    CANDIDATURA = 'CA'
    CIUDADANIA = 'CI'
    PERIODISTA = 'PE'
    ORGANO = 'OR'

    CARGOS = [
        (FUNCIONARIADO, 'Funcionariado público'),
        (CANDIDATURA, 'Precandidatura / Candidatura'),
        (CIUDADANIA, 'Ciudadanía'),
        (PERIODISTA, 'Periodista'),
        (ORGANO, 'Organo de dirección partidista'),
    ]
    
    cargo = models.CharField(
        max_length=2,
        choices=CARGOS,
        default=FUNCIONARIADO,
    )

    PARES = 'P'
    JERARQUICA = 'J'
    OPOSITOR = 'O'
    NINGUNA = 'N'
    OTRO = 'T'

    RELACIONES = [
        (PARES, 'Pares'),
        (JERARQUICA, 'Jerárquica'),
        (OPOSITOR, 'Opositor en la contienda'),
        (NINGUNA, 'Ninguna'),
        (OTRO, 'Otro'),
    ]

    relacion = models.CharField(
        max_length=1,
        choices=RELACIONES,
        default=PARES,
    )

    partido = models.CharField(max_length=255, null=True, blank=True)

    # INCIDENCIA
    NACIONAL = 'NA'
    ESTATAL = 'ES'
    MUNICIPAL = 'MU'

    AMBITOS = [
        (NACIONAL, 'Nacional'),
        (ESTATAL, 'Estatal'),
        (MUNICIPAL, 'Municipal'),
    ]
    
    ambito = models.CharField(
        max_length=2,
        choices=AMBITOS,
        default=ESTATAL,
    )

    incidencia = models.CharField(max_length=255, null=True)

    # DATOS DE IDENTIFICACION DE LA RESOLUCION O SENTENCIA FIRME O EJECUTORIADA
    expediente = models.CharField(max_length=255, null=True)
    organo = models.CharField(max_length=255, null=True)
    fecha_resolucion = models.DateField(null=True)    
    conducta = models.TextField(blank=True)
    
    NINGUNA = 'NI'
    AMONESTACION = 'AM'
    MULTA = 'MU'
    VISTA = 'VI'
    PERDIDA = 'PE'

    SANCIONES = [
        (NINGUNA, 'Ninguna'),
        (AMONESTACION, 'Amonestación Pública'),
        (MULTA, 'Multa Económica'),
        (VISTA, 'Vista al superior jerárquico para que sancione'),
        (PERDIDA, 'Pérdida del modo honesto de vivir, desde la emisión de la sentencia y hasta el próximo proceso electoral local y federal'),
    ]
    
    sancion = models.CharField(
        max_length=2,
        choices=SANCIONES,
        default=NINGUNA,
    )

    NO_APLICA = 'NA'
    LEVE = 'LE'
    GRAVE = 'GO'

    CALIFICACIONES = [
        (NO_APLICA, 'No Aplica'),
        (LEVE, 'Leve Especial'),
        (GRAVE, 'Grave Ordinaria'),
    ]
    
    calificacion = models.CharField(
        max_length=2,
        choices=CALIFICACIONES,
        default=NO_APLICA,
    )

    enlace = models.CharField(max_length=255, blank=True)

    # PERMANENCIA DE LA PERSONA SANCIONADA EN EL REGISTRO
    permanencia = models.DateField(null=True) 

    # IDENTIFICACION DEL FOLIO EN QUE SE REGISTRO LA RESOLUCION REINCIDENTE
    REINCIDENCIAS = [
        ('SI', 'Si'),
        ('NO', 'No'),
    ]

    reincidencia = models.CharField(
        max_length=2,
        choices=REINCIDENCIAS,
        default='NO',
    )

    # LA RESOLUCIÓN DETERMINÓ LA PERDIDA DE LA PRESUNCIÓN DEL MODO HONESTO DE VIVIR
    resolucion = models.CharField(
        max_length=2,
        choices=REINCIDENCIAS,
        default='NO',
    )

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    # Guardar cambios en BD
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre