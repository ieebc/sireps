# Generated by Django 3.2.5 on 2021-07-21 14:04

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sireps', '0010_historicalregistro'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalregistro',
            name='clave',
            field=models.CharField(max_length=18, null=True, validators=[django.core.validators.MinLengthValidator(18)]),
        ),
        migrations.AlterField(
            model_name='historicalregistro',
            name='incidencia',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='registro',
            name='clave',
            field=models.CharField(max_length=18, null=True, validators=[django.core.validators.MinLengthValidator(18)]),
        ),
        migrations.AlterField(
            model_name='registro',
            name='incidencia',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
