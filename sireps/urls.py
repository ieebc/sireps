from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    
    #Home
    path('', views.registros, name='registros'),

    #CRUD
    path('registro/', views.registro_nuevo, name='registro-nuevo'),
    path('registro/<int:pk>/', views.registro_editar, name='registro-editar'),
    path('registro/<int:pk>/eliminar/', views.registro_eliminar, name='registro-eliminar'),
    
    path('registro-modal/<int:pk>/', views.registro_modal, name='registro-modal'),

    #Auth
    path('login/', LoginView.as_view(template_name='sireps/login.html', redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(template_name='sireps/logout.html'), name='logout'),

    #Exportar
    path('exportar/', views.exportar_registros_xls, name='exportar'),

    #Informacion Publica  
    path('registros/', views.registros_publicos, name='registros-publicos'),

    #API
    path('api/registros/', views.api_registros, name='registros-api'),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)