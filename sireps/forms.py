from django import forms
from django.forms.widgets import DateInput, TextInput
from .models import Registro

class DateInput(forms.DateInput):
    input_type = 'date'

class RegistroForm(forms.ModelForm):    
    class Meta:
        model = Registro
        fields = ['tipo', 'nombre', 'clave', 'sexo', 'cargo', 'relacion', 'partido', 'ambito', 'incidencia', 'expediente', 'organo', 'fecha_resolucion', 'conducta', 'sancion', 'calificacion','enlace', 'permanencia', 'reincidencia', 'resolucion']
        labels = {
            'tipo': 'Tipo de registro',
            'nombre': 'Nombre de la persona sancionada',
            'clave': 'Clave electoral de la persona sancionada',
            'sexo': 'Sexo de la persona sancionada',
            'cargo': 'Cargo desempeñado de la persona infractora al momento de la sanción',
            'relacion': 'Relación con la víctima',
            'partido': 'Partido político, coalición, postulante o candidatura independiente',
            'ambito': 'Ámbito territorial',
            'incidencia': 'Incidencia de la sanción en el proceso electoral, respecto de la persona sancionada',
            'expediente': 'Número de expediente',
            'organo': 'Órgano resolutor',
            'fecha_resolucion': 'Fecha de la resolución o sentencia firme o ejecutoriada',
            'conducta': 'Conducta por la que se ejerció violencia política contra la mujer en razón de género',
            'sancion': 'Sanción',
            'calificacion': 'Calificación de la sanción',
            'enlace': 'Enlace electrónico que permita visualizar la resolución o sentencia firme o ejecutoriada',
            'permanencia': 'Permanencia de la persona sancionada en el registro',
            'reincidencia': 'Reincidencia de la conducta',
            'resolucion': 'La resolución determinó la perdida de la presunción del modo honesto de vivir'
        }
        widgets = {
            'clave': TextInput(attrs={'class':'text-uppercase'}),
            'fecha_resolucion': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control','type': 'date'}),
            'permanencia': DateInput(format=('%Y-%m-%d'), attrs={'class':'form-control','type': 'date'}),
        }